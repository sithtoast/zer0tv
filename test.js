var gameURIArray = [[]];
var gameTitle = "";
var gameBoxArray = [];
var currentGameBox = "";

function grabGames() {
    // First grab a list of games from the search term and clear the grid.
    var searchedGame = document.getElementById("searchedThingHTML").value;
    $("#gameGrid").empty();
    var searchedGamesString = 'https://api.twitch.tv/kraken/search/games?query=' + searchedGame + '&client_id=o5n16enllu8dztrwc6yk15ncrxdcvc&callback=?&type=suggest';
    var URIGames = encodeURI(searchedGamesString);
    var resultingGames;
    var gameStreams;
    
    $.getJSON(URIGames, function(gameList) {
        resultingGames = gameList.games;

        for(i = 0; i < resultingGames.length; i++)
        {
            var gameTitle = resultingGames[i].name;
            var gameBox = resultingGames[i].box.medium;
            var gameStreamsURL = 'https://api.twitch.tv/kraken/streams/?game=' + gameTitle + '&client_id=o5n16enllu8dztrwc6yk15ncrxdcvc&callback=?';
            var gameStreamsURI = encodeURI(gameStreamsURL);
            gameURIArray.push([gameStreamsURI,gameBox]);
        }    
        
        for(var j = 0; j < gameURIArray.length; j++) 
        {
				var l = j - j;
				currentGameBox = gameURIArray[j][1];

				$.ajax({
					url: gameURIArray[j][l],
					type: 'get',
					dataType: 'json',
					cache: false,
					success: function(gamesOnSuccess) { console.log(j +" Current Game Box: "+ currentGameBox); gameStreams = gamesOnSuccess._total; gameTitle = gamesOnSuccess.streams[0].game; elementIDTitle = gameTitle.replace(/\s+/g, '-'); $("#gameGrid").append("<div class='col-sm-6 col-md-4' id="+elementIDTitle+"><div class='thumbnail'><div class='caption'><img src="+currentGameBox+"><h3 id='gameTitleID'>"+ gameTitle +"</h3><p id='streamsLive'><button type='button' class='btn btn-default' id="+elementIDTitle+"  onclick='grabStreams(this.id)'><i class='fa fa-twitch' aria-hidden='true'></i> Live Channels: " + gameStreams + "</button></p></div></div></div>"); },
					async:true,
					});
		}  
    });
    gameURIArray = [];
}

function grabStreams(t) {
	
    var searchedThing = t.replace(/\-/g,' ');
    console.log(searchedThing);
    
	$("#streamsList").empty();
    // Using Moment.js to get the current time in ISO format.
    var now = moment.utc().format();

    var resultingStreams;
    $.getJSON('https://api.twitch.tv/kraken/streams/?game=' + searchedThing + '&client_id=o5n16enllu8dztrwc6yk15ncrxdcvc&callback=?', function(rawData) {
        var totalStreams = rawData._total;

		if(totalStreams > 100) {
			var streamOffset = (totalStreams * .9);
			streamOffset = Math.round(streamOffset);
		}
			else streamOffset = 0;

        $.getJSON('https://api.twitch.tv/kraken/streams?game=' + searchedThing + '&client_id=o5n16enllu8dztrwc6yk15ncrxdcvc&limit=100&offset=' + streamOffset + '&callback=?', function(scrubbedData) {


            resultingStreams = scrubbedData.streams;
            console.log(resultingStreams);

            for(i = 0; i < resultingStreams.length; i++)
            {
                var link = resultingStreams[i].channel.url;
                var displayName = resultingStreams[i].channel.display_name;
                var channelStatus = resultingStreams[i].channel.status;
                var currentFollowers = resultingStreams[i].channel.followers;
                var currentViewers = resultingStreams[i].viewers;
                var channelViews = resultingStreams[i].channel.views;
                var liveOrVOD = resultingStreams[i].stream_type;
                if(resultingStreams[i].stream_type === "watch_party") {
	                liveOrVOD = "<div id='vod' class='circle'></div> VOD";
                }
                else liveOrVOD = "<div id='live' class='circle'></div> Live";
                var streamPreview = resultingStreams[i].preview.small;
                var streamStarted = moment.utc(resultingStreams[i].created_at).format();
                var streamMinutes = moment(now).diff(streamStarted, 'minutes');
                var streamDuration = moment.duration(streamMinutes, "minutes").format("h [hrs], m [min]");

                if(currentViewers < 15)
                {
                    $("#streamsList").append("<tr><td data-title='Channel'>" + displayName + "<br><button type='button' id="+displayName+" onclick='showStream1(this.id)'>1</button><button type='button' id="+displayName+" onclick='showStream2(this.id)'>2</button><button type='button' id="+displayName+" onclick='showStream3(this.id)'>3</button><button type='button' id="+displayName+" onclick='showStream4(this.id)'>4</button></td><td style='word-wrap: break-word' data-title='Status'>" + channelStatus + "</td><td data-title='Viewers'>" + currentViewers + "</td><td data-title='Followers'>" + currentFollowers + "</td><td data-title='Views'>" + channelViews + "</td><td data-title='Time'>" + streamDuration + "</td><td data-title='Live'>" + liveOrVOD + "</td></tr>");
                }
            }
        } )
    } )
}

function showStream(dN){
    var channelName = dN;
    console.log(channelName);
    $("#gameGrid").empty();
    new Twitch.Embed("twitch-embed", 
    {
        width: 854,
        height: 480,
        channel: channelName
      });
}

function showStream1(dN){
    var channelName = dN;
    console.log(channelName);
    $("#gameGrid").empty();
    $("#channel1").empty();
    new Twitch.Embed("channel1", 
    {
        width: 854,
        height: 480,
        channel: channelName
      });
}
function showStream2(dN){
    var channelName = dN;
    console.log(channelName);
    $("#gameGrid").empty();
    $("#channel2").empty();
    new Twitch.Embed("channel2", 
    {
        width: 854,
        height: 480,
        channel: channelName
      });
}
function showStream3(dN){
    var channelName = dN;
    console.log(channelName);
    $("#gameGrid").empty();
    $("#channel3").empty();
    new Twitch.Embed("channel3", 
    {
        width: 854,
        height: 480,
        channel: channelName
      });
}

function showStream4(dN){
    var channelName = dN;
    console.log(channelName);
    $("#gameGrid").empty();
    $("#channel4").empty();
    new Twitch.Embed("channel4", 
    {
        width: 854,
        height: 480,
        channel: channelName
      });
}

function noStreams() {
    $("#twitch-embed").empty();
}